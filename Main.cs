using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;

namespace Regit
{
    public partial class Form1 : Form
    {
        RegistryKey key=null;
        bool isFoundProViewAgent = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                GetPrimaryServerName();
                GetSSTPPath();
                GetTCPIP_PROJECT();
                bool displayLocalPort = bool.Parse(ConfigurationSettings.AppSettings["DISPLAY_LOCAL_PORT"].ToString());
                textBox5.Visible = displayLocalPort;
                lblLocalPort.Visible = displayLocalPort;
            }
            catch(Exception lex){
                MessageBox.Show(lex.ToString());
            }
        }

        private void GetPrimaryServerName()
        {
            string PrimaryServerPath = ConfigurationSettings.AppSettings["PrimaryServerPath"].ToString();
            key = Registry.LocalMachine.OpenSubKey(PrimaryServerPath, true);
            if (key == null)
            {
                string PrimaryProViewServerPath = ConfigurationSettings.AppSettings["PrimaryProViewServerPath"].ToString();
                key = Registry.LocalMachine.OpenSubKey(PrimaryProViewServerPath, true);
                isFoundProViewAgent = true;
                lblRegistryPath.Text = "ProViewAgent";
            }
            else
            {
                lblRegistryPath.Text = "ProAgent";
            }
            textBox1.Text = key.GetValue("Name").ToString();
        }
        private void GetSSTPPath()
        {
            if (isFoundProViewAgent)
            {
                string SSTPProViewPath = ConfigurationSettings.AppSettings["SSTPProViewPath"].ToString();
                key = Registry.LocalMachine.OpenSubKey(SSTPProViewPath, true);
            }
            else
            {
                string SSTPPath = ConfigurationSettings.AppSettings["SSTPPath"].ToString();
                key = Registry.LocalMachine.OpenSubKey(@SSTPPath, true);
            }
            textBox2.Text = key.GetValue("TerminalID").ToString();
        }
        private void GetTCPIP_PROJECT()
        {
            string TCPIP_PROJECT = ConfigurationSettings.AppSettings["TCPIP_PROJECT"].ToString();
            key = Registry.LocalMachine.OpenSubKey(@TCPIP_PROJECT, true);
            textBox3.Text = key.GetValue("REMOTEPEER").ToString();
            textBox4.Text = key.GetValue("PORTNUMBER").ToString();
            textBox5.Text = key.GetValue("LOCALPORT").ToString();
        }

        private void SetSSTPPath()
        {
            if (isFoundProViewAgent)
            {
                string SSTPProViewPath = ConfigurationSettings.AppSettings["SSTPProViewPath"].ToString();
                key = Registry.LocalMachine.OpenSubKey(SSTPProViewPath, true);
            }
            else
            {
                string SSTPPath = ConfigurationSettings.AppSettings["SSTPPath"].ToString();
                key = Registry.LocalMachine.OpenSubKey(SSTPPath, true);
            }
            key.SetValue("TerminalID", textBox2.Text);
        }
        private void btnSetKey_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBox2.Text))
                {
                    throw new Exception("TerminalID Must not empty.");
                }

                if (isFoundProViewAgent)
                {
                    string PrimaryProViewServerPath = ConfigurationSettings.AppSettings["PrimaryProViewServerPath"].ToString();
                    key = Registry.LocalMachine.OpenSubKey(PrimaryProViewServerPath, true);
                    key.SetValue("Name", textBox1.Text);

                    string SSTPProViewPath = ConfigurationSettings.AppSettings["SSTPProViewPath"].ToString();
                    key = Registry.LocalMachine.OpenSubKey(SSTPProViewPath, true);
                    key.SetValue("TerminalID", textBox2.Text);

                }
                else
                {
                    string PrimaryServerPath = ConfigurationSettings.AppSettings["PrimaryServerPath"].ToString();
                    key = Registry.LocalMachine.OpenSubKey(PrimaryServerPath, true);
                    key.SetValue("Name", textBox1.Text);

                    string SSTPPath = ConfigurationSettings.AppSettings["SSTPPath"].ToString();
                    key = Registry.LocalMachine.OpenSubKey(SSTPPath, true);
                    key.SetValue("TerminalID", textBox2.Text);
                }
                

                string TCPIP_PROJECT = ConfigurationSettings.AppSettings["TCPIP_PROJECT"].ToString();
                key = Registry.LocalMachine.OpenSubKey(TCPIP_PROJECT, true);
                key.SetValue("REMOTEPEER", textBox3.Text);
                key.SetValue("PORTNUMBER", textBox4.Text);
                if (lblLocalPort.Visible)
                {
                    key.SetValue("LOCALPORT", textBox5.Text);
                }
                SetMachineName(textBox2.Text);
                MessageBox.Show("success.");

                //key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\ATMeye", true);
                //key.SetValue("AtmID", textBox2.Text);
                // no go to allowlist.html
                // button3_Click(sender,e);
            }
            catch (Exception eb)
            {
                MessageBox.Show(string.Format("Error: {0}",eb.ToString()));
            }
     
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //String text = "CRM:CMD=START_STOP_|_SESSIONTIMER=4294967295_|_SYNCTYPE=3_|_SESSIONURL=http://prosales:9080/adserver?terminaltype=S0&terminal=T044</td>"+
            //    "CRM:CMD=START_STOP_|_SESSIONTIMER=4294967295_|_SYNCTYPE=3_|_SESSIONURL=http://prosales:9080/adserver?terminaltype=S0&terminal=robbie</td>";
            if (textBox2.Text == "")
            {
                MessageBox.Show("Please fill terminal ID");
                return;
            }

            FileStream s;
            StreamReader r;
            String text = "";
            StreamWriter w=null;

            try
            {
                s = new FileStream("c:\\PROTOPAS\\Web\\Allow\\AllowList.htm", FileMode.Open);
                r = new StreamReader(s);

                text = r.ReadToEnd();
                r.Close();
                w = new StreamWriter("c:\\PROTOPAS\\Web\\Allow\\AllowList.htm");
            }
            catch (Exception exc)
            {
                MessageBox.Show("Path error \n\n"+exc.ToString());
            }

            String key = "terminal=";
            Boolean check = false;
            Boolean check2 = false;
            Boolean exit = false;
        

            int i, j;
            int x = 0;

            for( i=0;i<text.Length;i++)
            {
                if (exit)
                    break;
                for ( j = 0; j < key.Length; j++)
                {
                    if (text[i + j] == key[j])
                        check = true;
                    else
                    {
                        check = false;
                        break;
                    }
                }
                if (check)
                {
                    check2 = false;
                    
                    for (j = 0; j < text.Length- (i+9); j++)
                    {
                        if (text[i + j + 9] == '<')
                        {
                            check2 = true;
                            x = i + j + 9;
                            break;
                        }
                    }
                    if (check2)
                    {
                        String outt = text.Substring(i+9,x-(i+9));
                        text = text.Replace(outt, textBox2.Text);
                        try
                        {
                            w.Write(text);
                            MessageBox.Show("successed.");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        w.Close();
                        
                        exit = true;
                    }
                }
            }
           
        }

        public static bool SetMachineName(string newName)
        {
            RegistryKey key = Registry.LocalMachine;

            string activeComputerName = "SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ActiveComputerName";
            RegistryKey activeCmpName = key.CreateSubKey(activeComputerName);
            activeCmpName.SetValue("ComputerName", newName);
            activeCmpName.Close();
            string computerName = "SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ComputerName";
            RegistryKey cmpName = key.CreateSubKey(computerName);
            cmpName.SetValue("ComputerName", newName);
            cmpName.Close();
            string _hostName = "SYSTEM\\CurrentControlSet\\services\\Tcpip\\Parameters\\";
            RegistryKey hostName = key.CreateSubKey(_hostName);
            hostName.SetValue("Hostname", newName);
            hostName.SetValue("NV Hostname", newName);
            hostName.Close();
            return true;
        }
    }
}